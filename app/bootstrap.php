<?php

use Nette\Application\Routers\Route;
use Tracy\Debugger;

include __DIR__ . '/../libs/Nette/loader.php';

$configurator = new Nette\Configurator;

$ips = array('127.0.0.1');
$ips = [];
$debug = in_array($_SERVER['REMOTE_ADDR'], $ips);

if ($debug) {
	Debugger::enable();
	Debugger::$maxDepth = 10;
	$configurator->setDebugMode(TRUE);
	
} else {
	$configurator->enableTracy(__DIR__ . '/../log');
	$configurator->setDebugMode($configurator::NONE);
	Debugger::enable(Debugger::PRODUCTION);
}

$configurator->setTempDirectory(__DIR__ . '/../temp');
$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->addDirectory(__DIR__ . '/../libs/dibi')
	->register();

$configurator->addConfig(__DIR__ . '/config.neon');
$container = $configurator->createContainer();
$container->getService('application')->errorPresenter = 'Front:Error';

$router = $container->getByType(Nette\Application\IRouter::class);
$router[] = new Route('', 'Front:Homepage:default'/*, Route::SECURED*/);
$router[] = new Route('firmy/', 'Front:Homepage:firms'/*, Route::SECURED*/);
$router[] = new Route('articles/5-nejcastejsich-chyb-u-zivotopisu', 'Front:Homepage:mistakes'/*, Route::SECURED*/);
$router[] = new Route('upcr/', 'Front:Homepage:upcr'/*, Route::SECURED*/);
$router[] = new Route('articles/ochrana-soukromi', 'Front:Homepage:privates'/*, Route::SECURED*/);
$router[] = new Route('ic/<ic>', 'Front:Homepage:ic'/*, Route::SECURED*/);
$router[] = new Route('sitemap', 'Front:Sitemap:default'/*, Route::SECURED*/);

return $container;