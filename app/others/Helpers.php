<?php

class Helpers
{

	public static function specialDateTime($datetime)
	{		
		
		$today = new DateTime();
		
		if ($today == new DateTime($datetime)) {


			return "dnes " . self::datetime($datetime, "d.m. v H:i");
		} else {

			if (date("Y", strtotime($datetime) == date("Y", time())))
				return date("d.m.Y H:i", strtotime($datetime));
			else
				return self::date($datetime);
		}
	}
	

}
