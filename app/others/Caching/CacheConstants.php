<?php

namespace Caching;

class CacheConstants
{
	const CACHE_COMPANY = 'company';

	public static function getCacheTypes()
	{
		return array(
			self::CACHE_COMPANY,
		);
	}

}
