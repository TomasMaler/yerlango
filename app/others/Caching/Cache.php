<?php

namespace Caching;

abstract class Cache
{

	const CACHE_DIR = 'temp/';
	const NAMESPACE_PREFIX = 'c-';
	const SYSTEM_GROUP = 'system';
	const HASH_SIZE = 10;
	const CACHE_TIME_DAY = 82800;
	const CACHE_TIME_PERSISTENT = -1;

	private static $cacheGroup = self::SYSTEM_GROUP;

	public static function get($namespace, $var, $callback, $variables = array(), $callbackVars = array(), $time = NULL, $refresh = FALSE)
	{
		$res = self::getCache($namespace, $var, $variables);
		if ($res === NULL || $refresh) {
			$res = is_callable($callback) ? call_user_func_array($callback, $callbackVars) : NULL;
			self::saveCache($namespace, $var, $res, NULL, $variables, $time);
		}

		return $res;
	}

	public static function gc($namespace, $var, array $variables = array())
	{
		return self::getCache($namespace, $var, $variables);
	}

	public static function sc($namespace, $var, $val, array $variables = array(), $time = NULL)
	{
		return self::saveCache($namespace, $var, $val, $variables, $time);
	}

	private static function getCache($namespace, $var, array $variables = array())
	{
		$storage = new \Nette\Caching\Storages\FileStorage(self::getCacheDir());
		$cache = new \Nette\Caching\Cache($storage, $namespace);
		$varName = self::getVarName($var, $variables);
		$cache = $cache->load($varName);
		return (isset($cache) ? $cache : NULL);
	}

	protected static function saveCache($namespace, $var, $val, array $variables = array(), $time = FALSE)
	{
		if (!$time) {
			$time = 1 * 24 * 60 * 60;
		}

		$storage = new \Nette\Caching\Storages\FileStorage(self::getCacheDir());
		$cache = new \Nette\Caching\Cache($storage, $namespace);
		return ($cache->save(self::getVarName($var, $variables), $val, [\Nette\Caching\Cache::EXPIRE => $time . ' seconds'])) ? TRUE : FALSE;
	}

	public static function getVarName($var, array $vars = array())
	{
		return (self::$cacheGroup . "_$var" . (count($vars) > 0 ? '_' . self::stringifyVariables($vars) : ''));
	}

	public static function stringifyVariables($variables)
	{
		return substr(sha1(json_encode($variables)), 0, self::HASH_SIZE);
	}

	private static function delTree($dir)
	{
		$files = glob($dir . '*', GLOB_MARK);
		foreach ($files as $file) {
			if (substr($file, -1) == '/') {
				self::delTree($file);
			} else {
				unlink($file);
			}
		}
		if (is_dir($dir)) {
			rmdir($dir);
		}
	}

	public static function removeCache($namespaceName = NULL)
	{
		$cacheDir = self::getCacheDir() . "_" . $namespaceName;

		if (is_dir($cacheDir)) {
			self::delTree($cacheDir);
		}

		return TRUE;
	}

	private static function getCacheDir()
	{
		return WWW_DIR . '/' . self::CACHE_DIR;
	}

}
