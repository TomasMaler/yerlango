<?php

namespace FrontModule\Models;

use Tabler;

//use Caching\Cache;
//use Caching\CacheConstants;

class CompanyRepository extends Repository
{

	public function __construct(\DibiConnection $database)
	{
		parent::__construct($database);
		$this->table = Tabler::COMPANY;
	}

	/* public function getAll()
	  {
	  if (!$data = Cache::gc(CacheConstants::CACHE_COMPANY, 'getAll')) {
	  $data = $this->findAllFluent("*")
	  ->fetchAll();

	  Cache::sc(CacheConstants::CACHE_COMPANY, 'getAll', $data);
	  }

	  return $data;
	  } */

	public function getItemByIc($ic)
	{
		return $this->findAllFluent("*")
				->where('ic = %s', $ic)
				->fetch();
	}

}
