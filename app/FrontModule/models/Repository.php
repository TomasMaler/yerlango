<?php

namespace FrontModule\Models;

use Caching\Cache;
use Caching\CacheConstants;

abstract class Repository
{

	/** @var Nette\Database\Context */
	public $db;
	public $table;
	public static $staticDatabase;

	public function __construct(\DibiConnection $database)
	{
		$this->db = $database;
		self::$staticDatabase = $database;
	}

	/*
	 * fluent
	 */

	public function findAllFluent($select = "*")
	{
		return $this->db->select($select)
				->from($this->table);
	}

	public function findFluent($id)
	{
		return $this->findAllFluent()
				->where($this->table . '.' . $this->getPrimary() . "=%i", $id);
	}

	public function findByFluent($by, $key)
	{
		return $this->findAllFluent()
				->where("$by='$key'");
	}

	public function dataSource()
	{
		return $this->findAllFluent()
				->toDataSource();
	}

	/*
	 * fetched
	 */

	public function findAll()
	{
		return $this->findAllFluent()
				->fetchAll();
	}

	public function findAllSorting()
	{
		return $this->findAllFluent()
				->orderBy('sorting, id')
				->fetchAll();
	}

	public function find($id)
	{
		return $this->findFluent($id)
				->fetch();
	}

	public function findBy($by, $key)
	{
		return $this->findByFluent($by, $key)
				->fetch();
	}

	public function fetchSingle($primary, $select)
	{
		return $this->db->select($select)->from($this->table)->where($this->getPrimary() . ' = %i ', $primary)
				->fetchSingle($select);
	}

	public function findPairs($key1 = 'id', $key2 = 'name', $order = FALSE, $addEmptyValue = FALSE)
	{
		$result = $this->findAllFluent();
		if ($order) {
			$result->orderBy($order);
		}
		$pairs = array();
		if ($addEmptyValue) {
			$pairs[NULL] = '---';
		}
		return $pairs + $result->fetchPairs($key1, $key2);
	}

	/*
	 * CRUD
	 */

	public function insertRow($data, $returnId = 1)
	{
		$res = $this->db->insert($this->table, $this->transformArray($data))->execute();
		if ($returnId) {
			return $this->db->getInsertId();
		} else {
			return $res;
		}
	}

	public function updateRow($data, $primary)
	{
		$this->db->update($this->table, $this->transformArray($data))
			->where($this->getPrimary() . "=%i", $primary)
			->execute();

		return $primary;
	}

	public function deleteRow($id, $key = FALSE)
	{
		if ($key) {
			return $this->db->delete($this->table)
					->where($key . ' = %s', $id)->execute();
		} else {
			return $this->db->delete($this->table)
					->where($this->getPrimary() . ' = %i', $id)->execute();
		}
	}

	protected function transformArray($data)
	{
		if (!$tableColumns = Cache::gc(CacheConstants::CACHE_TABLE_COLUMNS, 'tableColumns', array($this->table))) {
			foreach ($this->db->query('SHOW COLUMNS FROM ' . $this->table)->fetchAll() as $column) {
				$tableColumns[] = $column['Field'];
			}
			Cache::sc(CacheConstants::CACHE_TABLE_COLUMNS, 'tableColumns', $tableColumns, array($this->table));
		}

		$newArray = array();
		foreach ($data as $key => $value) {
			if (in_array($key, $tableColumns)) {
				$newArray[$key] = $value;
			}
		}

		return $newArray;
	}

	protected function getTableColumn(Nette\Database\Connection $db)
	{
		$cols = $db->getSupplementalDriver()->getColumns($this->table);
		if ($cols) {
			$inTable = array();
			foreach ($cols as $key => $col) {
				$inTable[] = $col["name"];
			}
			return $inTable;
		} else {
			return false;
		}
	}

	public function multiInsert($table, $data)
	{
		return $this->db->query('INSERT INTO [' . $table . '] %ex', $data);
	}

	protected function getPrimary()
	{
		if (!$data = Cache::gc(CacheConstants::CACHE_TABLE_COLUMNS, $this->table . '_keys')) {
			$res = $this->db->query("SHOW KEYS FROM " . $this->table . " WHERE Key_name = 'PRIMARY'")->fetch();
			$data = $res->Column_name;
			Cache::sc(CacheConstants::CACHE_TABLE_COLUMNS, $this->table . '_keys', $data);
		}

		return $data;
	}

	public function transformCzDateValuesToDb($form)
	{
		$values = $form->values;
		foreach ($form->getForm()->components as $key => $component) {
			if (!empty($component->control->attrs['class']) && strpos($component->control->attrs['class'], "datepicker") !== FALSE) {
				$values[$key] = ($component->value != FALSE) ?
					$this->getCzDateToDb($component->value) : null;
			}
		}
		return $values;
	}

	private function getCzDateToDb($date)
	{
		if ($date != '') {
			$datumCas = explode(" ", $date);
			$datum = explode(".", $datumCas[0]);
			if (count($datum) > 1) {
				$dateDatabase = $datum[2] . "-" . sprintf("%02d", $datum[1]) . "-" . sprintf("%02d", $datum[0]);
				if (isset($datumCas[1])) {
					$dateDatabase .= " " . $datumCas[1];
				}
			} else {
				$dateDatabase = $date;
			}

			return $dateDatabase;
		} else {

			return $date;
		}
	}

}
