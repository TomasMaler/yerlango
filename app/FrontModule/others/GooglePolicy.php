<?php

namespace FrontModule;

use Nette\Http\Session;

class GooglePolicy
{

	private $session;
	
	public function __construct(Session $session)
	{
		$this->session = $session;
	}
	
	public function canShow()
	{
		$session = self::getSession();
	
		return empty($session['submitted']);
	}

	public function submit()
	{
		$session = self::getSession();
		$session['submitted'] = new \DateTime();
	}
	
	private function getSession()
	{
		return $this->session->getSection('googlePolicy')->setExpiration('1 year');
	}

}
