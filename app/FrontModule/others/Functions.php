<?php

namespace FrontModule;

class Functions
{

	public static function getTopPositions()
	{
		return [
			'Řidič' => 'ridic',
			'Skladník' => 'skladnik',
			'Účetní' => 'ucetni',
			'Svářeč' => 'svarec',
			'Dělník' => 'delnik',
			'Operátor výroby' => 'operator-vyroby',
			'Prodavač/Prodavačka' => 'prodavac-prodavacka',
			'Obchodní zástupce' => 'obchodni-zastupce',
			'Konstruktér' => 'konstrukter',
			'Elektrikář' => 'elektrikar',
			'Údržbář' => 'udrzbar',
			'Servisní technik' => 'servisni-technik',
			'Operátor/Operátorka' => 'operator-operatorka',
			'Zámečník' => 'zamecnik',
			'Nákupčí' => 'nakupci',
			'Pokladní' => 'pokladni',
			'Technolog' => 'technolog',
			'Montážní dělník' => 'montazni-delnik',
			'Kontrola kvality' => 'kontrola-kvality',
			'Programátor' => 'programator',
			'Projektant' => 'projektant',
			'Seřizovač' => 'serizovac',
			'Asistent/Asistentka' => 'asistent-asistentka',
			'Technik' => 'technik',
			'Nástrojař' => 'nastrojar',
			'Montér' => 'monter',
			'Referent/Referentka' => 'referent-referentka',
			'Projektový manažer' => 'projektovy-manazer',
			'Kontrolor' => 'kontrolor',
			'Personalista' => 'personalista',
		];
	}

	public static function getTopEmployers()
	{
		return [
			'PMU CZ, a.s' => 'https://www.spravnykrok.cz/prace/spolecnosti/170608-pmu-cz-a-s.html',
			'Globus ČR, k.s.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170649-globus-cr-ks.html',
			'ELTODO, a.s.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170681-eltodo-a-s.html',
			'BILLA, spol. s r. o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170665-billa-spol-s-r-o.html',
			'DELMART s.r.o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170682-delmart-s-r-o.html',
			'Ředitelství silnic a dálnic ČR' => 'https://www.spravnykrok.cz/prace/spolecnosti/170708-reditelstvi-silnic-a-dalnic-cr.html',
			'SIMACEK HS, spol. s r.o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170702-simacek-hs-spol-s-r-o.html',
			'AVES Plus Praha' => 'https://www.spravnykrok.cz/prace/spolecnosti/170689-aves-plus-praha.html',
			'Lesy České republiky, s.p.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170714-lesy-ceske-republiky-sp.html',
			'Západočeské konzumní družstvo Sušice' => 'https://www.spravnykrok.cz/prace/spolecnosti/170617-zapadoceske-konzumni-druzstvo-susice.html',
			'ATALIAN CZ s.r.o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170738-atalian-cz-s-r-o.html',
			'XLMX obchodní s.r.o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170711-xlmx-obchodni-s-r-o.html',
			'ČD - Telematika a.s.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170765-cd-telematika-a-s.html',
			'BV elektronik s.r.o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170722-bv-elektronik-s-r-o.html',
			'CZ LOKO, a.s.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170658-cz-loko-a-s.html',
			'KYB Manufacturing Czech s.r.o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170766-kyb-manufacturing-czech-s-r-o.html',
			'AG Experts s.r.o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170684-ag-experts-s-r-o.html',
			'Office Food s.r.o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170719-office-food-s-r-o.html',
			'HENIG - security servis, s. r. o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170751-henig-security-servis-s-r-o.html',
			'UNEX a.s.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170550-unex-a-s.html',
			'Bernex Bimetall s.r.o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170733-bernex-bimetall-s-r-o.html',
			'STUEKEN s.r.o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170668-stueken-s-r-o.html',
			'GEMSTONED s.r.o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170710-gemstoned-s-r-o.html',
			'Lesy hl.m.Prahy' => 'https://www.spravnykrok.cz/prace/spolecnosti/170779-lesy-hlmprahy.html',
			'APB - PLZEŇ a.s.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170768-apb-plzen-a-s.html',
			'RP Dimension s.r.o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170701-rp-dimension-s-r-o.html',
			'JLV, a.s.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170671-jlv-a-s.html',
			'Snoeks Automotive CZ s.r.o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170647-snoeks-automotive-cz-s-r-o.html',
			'FRAENKISCHE CZ s.r.o.' => 'https://www.spravnykrok.cz/prace/spolecnosti/170758-fraenkische-cz-s-r-o.html',
		];
	}

	public static function getRegions()
	{
		$data = [
			'Zahraničí' => 'zahranici',
			'Praha' => 'praha',
			'Jihomoravský kraj' => 'brno',
			'Jihočeský kraj' => 'ceske-budejovice',
			'Vysočina' => 'jihlava',
			'Karlovarský kraj' => 'karlovy-vary',
			'Královéhradecký kraj' => 'hradec-kralove',
			'Liberecký kraj' => 'liberec',
			'Olomoucký kraj' => 'olomouc',
			'Moravskoslezský kraj' => 'ostrava',
			'Pardubický kraj' => 'pardubice',
			'Plzeňský kraj' => 'plzen',
			'Středočeský kraj' => 'stredni-cechy',
			'Ústecký kraj' => 'usti-nad-labem',
			'Zlínský kraj' => 'zlin',
			'Celá ČR' => 'cela-cr',
		];

		asort($data);

		return $data;
	}

	public static function getCategories()
	{
		$data = [
			'Administrativa' => 'administrativa',
			'Automotive' => 'automotive',
			'Bankovnictví a finanční služby' => 'bankovnictvi',
			'Brigády' => 'brigady',
			'Chemie a potravinářství' => 'chemie-potravinarstvi',
			'Cestovní ruch a ubytování' => 'cestovni-ruch-ubytovani',
			'Ekonomika' => 'ekonomika',
			'Elektrotechnické profese' => 'elektrotechnicke-profese',
			'Gastronomie a pohostinství' => 'gastronomie-pohostinstvi',
			'Informační technologie' => 'informacni-technologie',
			'Kvalita a kontrola jakosti' => 'kvalita-kontrola-jakosti',
			'Logistika a doprava' => 'logistika-doprava',
			'Manuální a dělnické profese' => 'manualni-delnicke-profese',
			'Management' => 'management',
			'Marketing a media' => 'marketing-media',
			'Nákup' => 'nakup',
			'Ostraha a bezpečnost' => 'ostraha-bezpecnost',
			'Obchod-prodej' => 'obchod-prodej',
			'Ostatní' => 'ostatni',
			'Personalistika a lidské zdroje' => 'personalistika-lidske-zdroje',
			'Právní služby' => 'pravni-sluzby',
			'Reality' => 'reality',
			'Řemeslo a umění' => 'remeslo-umeni',
			'Služby' => 'sluzby',
			'Strojírenství' => 'strojirenstvi',
			'Státní správa' => 'statni-sprava',
			'Stavebnictví' => 'stavebnictvi',
			'Telekomunikace' => 'telekomunikace',
			'Technické profese' => 'technicke-profese',
			'Výroba, průmysl a energetika' => 'vyroba-prumysl-energetika',
			'Vzdělávání, věda a vývoj' => 'vzdelavani-veda-vyvoj',
			'Zdravotnictví a farmacie' => 'zdravotnictvi-farmacie',
			'Zemědělství, lesnictví a ekologie' => 'zemedelstvi-lesnictvi-ekologie',
		];

		asort($data);

		return $data;
	}

	public static function shuffle($array)
	{
		$keys = array_keys($array);
		shuffle($keys);
		$random = [];
		foreach ($keys as $key) {
			$random[$key] = $array[$key];
		}

		return $random;
	}

	public static function getUpcrCities()
	{
		$data = [
			149 => 'Hlavní město Praha',
			101 => 'Benešov',
			102 => 'Beroun',
			126 => 'Kladno',
			128 => 'Kolín',
			130 => 'Kutná Hora',
			134 => 'Mělník',
			135 => 'Mladá Boleslav',
			139 => 'Nymburk',
			150 => 'Praha-východ',
			151 => 'Praha-západ',
			155 => 'Příbram',
			156 => 'Rakovník',
			109 => 'České Budějovice',
			110 => 'Český Krumlov',
			123 => 'Jindřichův Hradec',
			144 => 'Pelhřimov',
			145 => 'Písek',
			152 => 'Prachatice',
			161 => 'Strakonice',
			164 => 'Tábor',
			112 => 'Domažlice',
			117 => 'Cheb',
			124 => 'Karlovy Vary',
			127 => 'Klatovy',
			146 => 'Plzeň-město',
			147 => 'Plzeň-jih',
			148 => 'Plzeň-sever',
			157 => 'Rokycany',
			160 => 'Sokolov',
			165 => 'Tachov',
			108 => 'Česká Lípa',
			111 => 'Děčín',
			118 => 'Chomutov',
			120 => 'Jablonec nad Nisou',
			131 => 'Liberec',
			132 => 'Litoměřice',
			133 => 'Louny',
			136 => 'Most',
			166 => 'Teplice',
			170 => 'Ústí nad Labem',
			114 => 'Havlíčkův Brod',
			116 => 'Hradec Králové',
			119 => 'Chrudim',
			121 => 'Jičín',
			137 => 'Náchod',
			143 => 'Pardubice',
			158 => 'Rychnov nad Kněžnou',
			159 => 'Semily',
			162 => 'Svitavy',
			167 => 'Trutnov',
			171 => 'Ústí nad Orlicí',
			103 => 'Blansko',
			104 => 'Brno-město',
			105 => 'Brno-venkov',
			107 => 'Břeclav',
			174 => 'Zlín',
			115 => 'Hodonín',
			122 => 'Jihlava',
			129 => 'Kroměříž',
			153 => 'Prostějov',
			168 => 'Třebíč',
			169 => 'Uherské Hradiště',
			173 => 'Vyškov',
			175 => 'Znojmo',
			176 => 'Žďár nad Sázavou',
			106 => 'Bruntál',
			113 => 'Frýdek-Místek',
			125 => 'Karviná',
			138 => 'Nový Jičín',
			140 => 'Olomouc',
			141 => 'Opava',
			142 => 'Ostrava-město',
			154 => 'Přerov',
			163 => 'Šumperk',
			172 => 'Vsetín',
			177 => 'Jeseník',
			500 => 'Práce v zahraničí',
		];
		
		asort($data);

		return $data;
	}

}
