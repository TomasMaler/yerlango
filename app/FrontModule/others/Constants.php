<?php

namespace FrontModule;

class Constants
{

	const JOBSIK_SEARCH_URL = 'https://www.jobsik.cz/search/';
	const DP_SEARCH_URL = 'https://www.dobraprace.cz/';
	const JOB_TYPE_ID_OFFERS = 1;

}
