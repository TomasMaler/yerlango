<?php

namespace FrontModule;

use FrontModule\Models\CompanyRepository;

class HomepagePresenter extends BasePresenter
{

	/** @var CompanyRepository @inject * */
	public $company;

	public function beforeRender()
	{
		parent::beforeRender();
	}

	public function actionDefault()
	{
		$this->seoSource = [
			'title' => 'Najděte si práci',
			//'h1' => '',
			//"title" => '',
			//"description" => '',
			//"keywords" => '',
		];
	}

	public function actionFirms()
	{
		$this->seoSource = [
			'title' => 'Hledáte zaměstnance?',
		];
	}

	public function actionMistakes()
	{
		$this->seoSource = [
			'title' => 'Pět nejčastějších chyb při psaní životopisu',
		];
	}

	public function actionUpcr()
	{
		$this->seoSource = [
			'title' => 'Nabídky práce z úřadů práce',
		];

		$this->template->upcr = Functions::getUpcrCities();
	}

	public function actionPrivates()
	{
		$this->seoSource = [
			'title' => 'Pravidla ochrany soukromí',
		];
	}

	public function actionIc($ic)
	{
		if (empty($ic)) {
			$this->forward('Error:default', new \Nette\Application\BadRequestException);
		}
		$companyData = $this->company->getItemByIc($ic);
		if (empty($companyData)) {
			$this->forward('Error:default', new \Nette\Application\BadRequestException);
		}
		$this->template->company = $companyData;
		$this->seoSource = [
			'title' => 'Nabídky práce firmy ' . $companyData['name'] . ', ič.: ' . $ic
		];
	}

}
