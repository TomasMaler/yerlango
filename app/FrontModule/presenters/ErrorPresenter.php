<?php

namespace FrontModule;

class ErrorPresenter extends BasePresenter
{

	public function actionDefault(\Exception $exception)
	{
		if ($this->isAjax()) {
			$this->payload->error = TRUE;
			$this->terminate();
		} elseif ($exception instanceof \Nette\Application\BadRequestException) {
			$this->seoSource = array('title' => 'Pracovní pozice');
			$this->setView('404');
			$this->getHttpResponse()->setCode('200');
			//$this->redirectUrl('https://www.jobsik.cz/volna-pracovni-mista/', \Nette\Http\IResponse::S301_MOVED_PERMANENTLY);
		} else {
			$this->seoSource = array('title' => 'Na portálu Yerlango.com se vyskytla chyba');
			$this->setView('500');
			$this->getHttpResponse()->setCode('500');
		}
	}

}
