<?php

namespace FrontModule;

use Nette\Application\UI\Presenter;
use FrontModule\GooglePolicy;
use Nette\Application\UI\Form;
use Nette\Utils\Strings AS NStrings;

abstract class BasePresenter extends Presenter
{

	/** @var GooglePolicy @inject * */
	public $googlePolicy;
	protected $seoSource;
	protected $appSetting;
	protected $breadcrumbs;
	protected $seo;

	protected function startup()
	{
		parent::startup();

		$this->appSetting = (array) $this->context->parameters['appSetting'];
		$this->template->appSetting = $this->appSetting;
		$this->template->showGooglePolicy = $this->googlePolicy->canShow();
	}

	public function handleCookieConfirm()
	{
		$this->googlePolicy->submit();
		$this->template->showGooglePolicy = FALSE;
		$this->invalidateControl("cookieConfirmed");
	}

	protected function beforeRender()
	{
		parent::beforeRender();

		$this->getSeo();
		
		$this->template->breadcrumbs = $this->breadcrumbs;
		$this->template->seo = $this->seo;
		$this->template->getLatte()->addFilter('specialDateTime', 'Helpers::specialDateTime');
	}

	protected function addBreadcrumb($name, $link)
	{
		$this->breadcrumbs[] = ['name' => $name, 'link' => $link];
	}

	protected function getSeo()
	{
		$seoSource = $this->seoSource;

		if (empty($seoSource)) {
			$seoSource['title'] = $this->appSetting['title'];
		}

		$seo = [];
		$seo['meta_title'] = (isset($seoSource['meta_title'])) ? $seoSource['meta_title'] : $seoSource['title'];
		$seo['description'] = (isset($seoSource['description'])) ? $seoSource['description'] : $seoSource['title'];
		$seo['keywords'] = (isset($seoSource['keywords'])) ? $seoSource['keywords'] : $seoSource['title'];
		$seo['h1'] = (isset($seoSource['h1'])) ? $seoSource['h1'] : $seoSource['title'];
		//$seo['meta_title'] .= ' | ' . $this->appSetting['title'];

		$this->seo = $seo;
	}

	protected function createComponentHeader()
	{
		$header = new \HeaderControl();
		
		$header->setDocType(\HeaderControl::HTML_5);
		$header->setLanguage(\HeaderControl::CZECH);
		$header->setTitleSeparator(' | ')
			->setTitle($this->seo['meta_title'])
			->setAuthor('INET-SERVIS.cz')
			->setDescription($this->seo['description'])
			->addKeywords($this->seo['keywords'])
			->setRobots('index,follow');

		$header->setMetaTag("viewport", "width=device-width,initial-scale=1");

		$url = new \Nette\Http\Url($this->link("//this"));
		$header->addRel("canonical", $url->hostUrl . $url->path);

		$css = $header['css'];
		$css->setSourcePath(WWW_DIR . '/css');
		$css->setTempUri(URL . 'temp');
		$css->setTempPath(WWW_DIR . '/temp');

		$js = $header['js'];
		$js->setTempPath(WWW_DIR . '/temp');
		$js->setTempUri(URL . 'temp');
		$js->setSourcePath(WWW_DIR . '/js');

		$css->addFile('reset.css');
		$css->addFile('screen-bootstrap.css');
		$css->addFile('screen.css');

		$js->addFile('jquery-1.10.1.min.js');
		$js->addFile('jquery.nette.js');
		$js->addFile('netteForms.js');
		$js->addFile('jquery.livequery.js');
		$js->addFile('ajax.js');

		return $header;
	}

	public function createComponentSearchForm()
	{
		$form = new Form();
		$form->addGroup();
		$form->addSelect('category', 'Vyberte obor', array_flip(Functions::getCategories()))->setPrompt('-- obor --');
		$form->addSelect('region', 'Vyberte region', array_flip(Functions::getRegions()))->setPrompt('-- region --');

		$form->addSubmit('save', 'Hledat pracovní pozice')->getControlPrototype()->class('button cyan');
		$form->onSuccess[] = [$this, 'searchFormSubmitted'];

		return $form;
	}

	public function searchFormSubmitted(Form $form)
	{
		$data = $form->values;

		$url = \FrontModule\Constants::DP_SEARCH_URL;

		if (!empty($data['region']) && !empty($data['category'])) {
			$url .= $data['category'] . '/' .  $data['region'] . '/';
		} else if (!empty($data['region']) && !empty($data['category'])) {
			$url .= 'nabidka-prace/' . $data['region'] . '/';
		} else if (!empty($data['category'])) {
			$url .= $data['category'] . '/';
		}
		
		$this->redirectUrl($url/* . '&utm=yerlango'*/);//urm nejede
	}
	
	public function createComponentContactForm()
	{
		$form = new Form();
		$form->addGroup();
		$form->addText('name', 'Jméno a příjmení')->setRequired('Vyplňte Jméno a příjmení');
		$form->addText('email', 'Email')->setRequired('Vyplňte Email')->addRule(Form::EMAIL, 'Vyplňte Email');
		$form->addText('phone', 'Telefon')->setRequired('Vyplňte Telefon');
		$form->addTextArea('text', 'Text')->setRequired('Vyplňte Text');
		$form->addText('captcha', 'Captcha')->setOption('description', \Nette\Utils\Html::el('span')->setText('Vyplňte prosím název tohoto webu bez diakritiky'));

		$form->addSubmit('save', 'Odeslat')->getControlPrototype()->class('button');
		$form->onSuccess[] = [$this, 'contactFormSubmitted'];

		return $form;
	}

	public function contactFormSubmitted(Form $form)
	{
		$data = $form->values;
		
		$captcha = str_replace(' ', '', $data['captcha']);
		$captcha = str_replace('.cz', '', $data['captcha']);
		
		if (!in_array($captcha, ['Yerlango', 'yerlango'])) {
			$form->addError('Vyplňte prosím název webu: yerlango');
			return;
		}
		
		$app = $this->appSetting;

                $mail = new \Nette\Mail\Message;
                $mail->setSubject('Yerlango.cz - dotaz - ' . $data['name']);
                $mail->setFrom($data['email']);

                $mail->addTo($this->appSetting['contactFormEmail']);

                $template = $this->createTemplate();
                $template->setFile(APP_DIR . '/FrontModule/templates/Mail/contactInfoFormMail.latte');
                $template->data = $data;
                $template->app = $app;

		$mail->setHtmlBody($template);

                $sender = new \Nette\Mail\SendmailMailer();
                $sender->send($mail);
		
		$this->flashMessage('Dotaz byl odeslán. Děkujeme.');
		$this->redirect('this');
	}

}
