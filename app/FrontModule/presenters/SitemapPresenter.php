<?php

namespace FrontModule;

class SitemapPresenter extends BasePresenter {

        public function actionDefault()
        {
                $this->seoSource = "";
                $this->template->data = $this->generateSitemap();
                $httpResponse = \Nette\Environment::getHttpResponse();
                $httpResponse->setContentType('xml', 'UTF-8');
        }

        private function generateSitemap()
        {
                $defaut_urls = array(
                        '',
                        'firmy/',
                        'articles/5-nejcastejsich-chyb-u-zivotopisu',
			'articles/ochrana-soukromi',
			'upcr/',
                );
	
                $data = array();

                $i = 0;
                foreach ($defaut_urls as $item) {
                        $data[$i]['loc'] = URL . $item;
                        $data[$i]['changefreq'] = 'daily';
                        $data[$i]['priority'] = '1';
                        $i++;
                }

                return $data;
        }

}
