<?php

/*if ($_SERVER["REMOTE_ADDR"] != "88.146.158.146") {
       die("Omlouváme se, přecházíme na novou verzi webu.");
}*/

// absolute filesystem path to this web root
define('WWW_DIR', __DIR__);

// absolute filesystem path to the application root
define('APP_DIR', WWW_DIR . '/app');
// URL
define('URL', ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') ? 'https' : 'http') . '://' . $_SERVER['SERVER_NAME'].'/');

// let bootstrap create Dependency Injection container
$container = require __DIR__ . '/app/bootstrap.php';

// run application
$container->getService('application')->run();